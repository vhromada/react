# DB

Before you start you have to install [Derby DB 10.10.2.0](http://db.apache.org/derby/derby_downloads.html)

When you are done just run

1. `ij.bat` from directory with Derby DB to start console
2. type `connect 'jdbc:derby:Books;create=true';` in console to create database Books
3. type `run '{full path to table.sql}'` in console to create tables in database
4. type `run '{full path to data.sql}'` in console to insert data to database
5. type `disconnect;` in console to close connection to database
6. `startNetworkServer.bat` from directory with Derby DB to start database

--DROP SEQUENCE languages_sq RESTRICT;
--DROP SEQUENCE books_sq RESTRICT;

CREATE SEQUENCE languages_sq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE books_sq START WITH 1 INCREMENT BY 1;

--DROP TABLE books;
--DROP TABLE languages;

CREATE TABLE languages (
  id BIGINT NOT NULL CONSTRAINT languages_pk PRIMARY KEY,
  language_value VARCHAR(100) NOT NULL CONSTRAINT languages_language_value_ck CHECK (LENGTH(language_value) > 0)
);

CREATE TABLE books (
  id BIGINT NOT NULL CONSTRAINT books_pk PRIMARY KEY,
  language_value BIGINT NOT NULL CONSTRAINT books_language_value_fk REFERENCES languages (id),
  author VARCHAR(200) NOT NULL CONSTRAINT books_author_ck CHECK (LENGTH(author) > 0),
  title VARCHAR(200) NOT NULL CONSTRAINT books_title_ck CHECK (LENGTH(title) > 0),
  issue_year INTEGER NOT NULL CONSTRAINT books_issue_year_ck CHECK (issue_year > 1000),
  paper_form BOOLEAN NOT NULL,
  note VARCHAR(500)
);

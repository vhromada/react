INSERT INTO languages (id, language_value) VALUES (NEXT VALUE FOR languages_sq, 'Czech');
INSERT INTO languages (id, language_value) VALUES (NEXT VALUE FOR languages_sq, 'Slovak');
INSERT INTO languages (id, language_value) VALUES (NEXT VALUE FOR languages_sq, 'English');
INSERT INTO languages (id, language_value) VALUES (NEXT VALUE FOR languages_sq, 'French');
INSERT INTO languages (id, language_value) VALUES (NEXT VALUE FOR languages_sq, 'German');
INSERT INTO languages (id, language_value) VALUES (NEXT VALUE FOR languages_sq, 'Russian');
INSERT INTO languages (id, language_value) VALUES (NEXT VALUE FOR languages_sq, 'Italian');
INSERT INTO languages (id, language_value) VALUES (NEXT VALUE FOR languages_sq, 'Spanish');

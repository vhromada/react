package cz.vhromada.books.to;

/**
 * A class represents TO for language.
 *
 * @author Vladimir Hromada
 */
public class LanguageTO {

	private Long id;

	private String value;

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(final String value) {
		this.value = value;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		final LanguageTO language = (LanguageTO) o;

		return id != null ? id.equals(language.id) : language.id == null;
	}

	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : 0;
	}

	@Override
	public String toString() {
		return String.format("LanguageTO [id=%d, value=%s]", id, value);
	}

}

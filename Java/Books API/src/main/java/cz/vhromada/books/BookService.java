package cz.vhromada.books;

import java.util.List;

import cz.vhromada.books.to.BookTO;

/**
 * A class represents API for service for books.
 *
 * @author Vladimir Hromada
 */
public interface BookService {

	/**
	 * Returns list of books.
	 *
	 * @return list of books
	 */
	List<BookTO> getBooks();

	/**
	 * Returns book with ID or null if there isn't such book.
	 *
	 * @param id ID
	 * @return book with ID or null if there isn't such book
	 * @throws IllegalArgumentException if ID is null
	 */
	BookTO getBook(Long id);

	/**
	 * Adds book. Sets new ID and position.
	 *
	 * @param book book
	 * @throws IllegalArgumentException if book is null
	 */
	void add(BookTO book);

	/**
	 * Updates book.
	 *
	 * @param book book
	 * @throws IllegalArgumentException if book is null
	 */
	void update(BookTO book);

	/**
	 * Removes book.
	 *
	 * @param book book
	 * @throws IllegalArgumentException if book is null
	 */
	void remove(BookTO book);

}

package cz.vhromada.books.to;

/**
 * A class represents TO for book.
 *
 * @author Vladimir Hromada
 */
public class BookTO {

	private Long id;

	private String author;

	private String title;

	private int issueYear;

	private LanguageTO language;

	private Boolean paperForm;

	private String note;

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(final String author) {
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public int getIssueYear() {
		return issueYear;
	}

	public void setIssueYear(final int issueYear) {
		this.issueYear = issueYear;
	}

	public LanguageTO getLanguage() {
		return language;
	}

	public void setLanguage(final LanguageTO language) {
		this.language = language;
	}

	public Boolean getPaperForm() {
		return paperForm;
	}

	public void setPaperForm(final Boolean paperForm) {
		this.paperForm = paperForm;
	}

	public String getNote() {
		return note;
	}

	public void setNote(final String note) {
		this.note = note;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		final BookTO book = (BookTO) o;

		return id != null ? id.equals(book.id) : book.id == null;
	}

	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : 0;
	}

	@Override
	public String toString() {
		return String.format("BookTO [id=%d, author=%s, title=%s, issueYear=%d, language=%s, paperForm=%b, note=%s]", id, author, title, issueYear, language,
				paperForm, note);
	}

}

package cz.vhromada.books;

import java.util.List;

import cz.vhromada.books.to.LanguageTO;

/**
 * A class represents API for service for languages.
 *
 * @author Vladimir Hromada
 */
public interface LanguageService {

	/**
	 * Returns list of languages.
	 *
	 * @return list of languages
	 */
	List<LanguageTO> getLanguages();

	/**
	 * Returns language with ID or null if there isn't such language.
	 *
	 * @param id ID
	 * @return language with ID or null if there isn't such language
	 * @throws IllegalArgumentException if ID is null
	 */
	LanguageTO getLanguage(Long id);

	/**
	 * Adds language. Sets new ID and position.
	 *
	 * @param language language
	 * @throws IllegalArgumentException if language is null
	 */
	void add(LanguageTO language);

	/**
	 * Updates language.
	 *
	 * @param language language
	 * @throws IllegalArgumentException if language is null
	 */
	void update(LanguageTO language);

	/**
	 * Removes language.
	 *
	 * @param language language
	 * @throws IllegalArgumentException if language is null
	 */
	void remove(LanguageTO language);

}

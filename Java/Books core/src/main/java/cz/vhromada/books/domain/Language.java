package cz.vhromada.books.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * A class represents language.
 *
 * @author Vladimir Hromada
 */
@Entity
@Table(name = "languages")
public class Language implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "language_generator", sequenceName = "languages_sq", allocationSize = 0)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "language_generator")
	private Long id;

	@Column(name = "language_value")
	private String value;

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(final String value) {
		this.value = value;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass() || id == null) {
			return false;
		}

		final Language language = (Language) o;

		return id.equals(language.id);
	}

	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : 0;
	}

	@Override
	public String toString() {
		return String.format("Language [id=%d, value=%s]", id, value);
	}

}

package cz.vhromada.books.converter;

import cz.vhromada.books.domain.Language;
import cz.vhromada.books.to.LanguageTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * A class represents converter from {@link Language} to {@link LanguageTO}.
 *
 * @author Vladimir Hromada
 */
@Component("languageTOConverter")
public class LanguageTOConverter implements Converter<Language, LanguageTO> {

	@Override
	public LanguageTO convert(final Language source) {
		if (source == null) {
			return null;
		}

		final LanguageTO language = new LanguageTO();
		language.setId(source.getId());
		language.setValue(source.getValue());

		return language;
	}

}

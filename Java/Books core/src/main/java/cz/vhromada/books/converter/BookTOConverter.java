package cz.vhromada.books.converter;

import cz.vhromada.books.domain.Book;
import cz.vhromada.books.to.BookTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * A class represents converter from {@link Book} to {@link BookTO}.
 *
 * @author Vladimir Hromada
 */
@Component("bookTOConverter")
public class BookTOConverter implements Converter<Book, BookTO> {

	@Autowired
	private LanguageTOConverter languageConverter;

	@Override
	public BookTO convert(final Book source) {
		if (source == null) {
			return null;
		}

		final BookTO book = new BookTO();
		book.setId(source.getId());
		book.setAuthor(source.getAuthor());
		book.setTitle(source.getTitle());
		book.setIssueYear(source.getIssueYear());
		book.setLanguage(languageConverter.convert(source.getLanguage()));
		book.setPaperForm(source.getPaperForm());
		book.setNote(source.getNote());

		return book;
	}

}

package cz.vhromada.books.service;

import java.util.ArrayList;
import java.util.List;

import cz.vhromada.books.LanguageService;
import cz.vhromada.books.dao.LanguageDao;
import cz.vhromada.books.domain.Language;
import cz.vhromada.books.to.LanguageTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * A class represents implementation of service for languages.
 *
 * @author Vladimir Hromada
 */
@Component("languageService")
@Transactional
public class LanguageServiceImpl implements LanguageService {

	/** DAO for languages */
	@Autowired
	private LanguageDao languageDao;

	/** Conversion service */
	@Autowired
	private ConversionService conversionService;

	@Override
	@Transactional(readOnly = true)
	public List<LanguageTO> getLanguages() {
		final List<LanguageTO> languages = new ArrayList<LanguageTO>();
		for (Language language : languageDao.getLanguages()) {
			languages.add(conversionService.convert(language, LanguageTO.class));
		}

		return languages;
	}

	@Override
	@Transactional(readOnly = true)
	public LanguageTO getLanguage(final Long id) {
		return conversionService.convert(languageDao.getLanguage(id), LanguageTO.class);
	}

	@Override
	public void add(final LanguageTO language) {
		languageDao.add(conversionService.convert(language, Language.class));
	}

	@Override
	public void update(final LanguageTO language) {
		languageDao.update(conversionService.convert(language, Language.class));
	}

	@Override
	public void remove(final LanguageTO language) {
		languageDao.remove(conversionService.convert(language, Language.class));
	}

}

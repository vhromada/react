package cz.vhromada.books.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import cz.vhromada.books.domain.Language;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * A class represents implementation of DAO for languages.
 *
 * @author Vladimir Hromada
 */
@Repository("languageDAO")
public class LanguageDaoImpl implements LanguageDao {

	/** Entity manager */
	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Language> getLanguages() {
		return new ArrayList<Language>(entityManager.createQuery("SELECT l FROM Language l ORDER BY id", Language.class).getResultList());
	}

	@Override
	public Language getLanguage(final Long id) {
		if (id == null) {
			throw new IllegalArgumentException("ID mustn't be null.");
		}

		return entityManager.find(Language.class, id);
	}

	@Override
	public void add(final Language language) {
		if (language == null) {
			throw new IllegalArgumentException("Language mustn't be null.");
		}

		entityManager.persist(language);
	}

	@Override
	public void update(final Language language) {
		if (language == null) {
			throw new IllegalArgumentException("Language mustn't be null.");
		}

		entityManager.merge(language);
	}

	@Override
	public void remove(final Language language) {
		if (language == null) {
			throw new IllegalArgumentException("Language mustn't be null.");
		}

		if (entityManager.contains(language)) {
			entityManager.remove(language);
		} else {
			entityManager.remove(entityManager.getReference(Language.class, language.getId()));
		}
	}

}

package cz.vhromada.books.service;

import java.util.ArrayList;
import java.util.List;

import cz.vhromada.books.BookService;
import cz.vhromada.books.dao.BookDao;
import cz.vhromada.books.domain.Book;
import cz.vhromada.books.to.BookTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * A class represents implementation of service for books.
 *
 * @author Vladimir Hromada
 */
@Component("bookService")
@Transactional
public class BookServiceImpl implements BookService {

	/** DAO for books */
	@Autowired
	private BookDao bookDao;

	/** Conversion service */
	@Autowired
	private ConversionService conversionService;

	@Override
	@Transactional(readOnly = true)
	public List<BookTO> getBooks() {
		final List<BookTO> books = new ArrayList<BookTO>();
		for (Book book : bookDao.getBooks()) {
			books.add(conversionService.convert(book, BookTO.class));
		}

		return books;
	}

	@Override
	@Transactional(readOnly = true)
	public BookTO getBook(final Long id) {
		return conversionService.convert(bookDao.getBook(id), BookTO.class);
	}

	@Override
	public void add(final BookTO book) {
		bookDao.add(conversionService.convert(book, Book.class));
	}

	@Override
	public void update(final BookTO book) {
		bookDao.update(conversionService.convert(book, Book.class));
	}

	@Override
	public void remove(final BookTO book) {
		bookDao.remove(conversionService.convert(book, Book.class));
	}

}

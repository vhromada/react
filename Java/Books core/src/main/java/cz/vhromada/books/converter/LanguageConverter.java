package cz.vhromada.books.converter;

import cz.vhromada.books.domain.Language;
import cz.vhromada.books.to.LanguageTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * A class represents converter from {@link LanguageTO} to {@link Language}.
 *
 * @author Vladimir Hromada
 */
@Component("languageConverter")
public class LanguageConverter implements Converter<LanguageTO, Language> {

	@Override
	public Language convert(final LanguageTO source) {
		if (source == null) {
			return null;
		}

		final Language language = new Language();
		language.setId(source.getId());
		language.setValue(source.getValue());

		return language;
	}

}

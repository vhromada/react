package cz.vhromada.books.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import cz.vhromada.books.domain.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * A class represents implementation of DAO for books.
 *
 * @author Vladimir Hromada
 */
@Repository("bookDAO")
public class BookDaoImpl implements BookDao {

	/** Entity manager */
	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Book> getBooks() {
		return new ArrayList<Book>(entityManager.createQuery("SELECT b FROM Book b ORDER BY id", Book.class).getResultList());
	}

	@Override
	public Book getBook(final Long id) {
		if (id == null) {
			throw new IllegalArgumentException("ID mustn't be null.");
		}

		return entityManager.find(Book.class, id);
	}

	@Override
	public void add(final Book book) {
		if (book == null) {
			throw new IllegalArgumentException("Book mustn't be null.");
		}

		entityManager.persist(book);
	}

	@Override
	public void update(final Book book) {
		if (book == null) {
			throw new IllegalArgumentException("Book mustn't be null.");
		}

		entityManager.merge(book);
	}

	@Override
	public void remove(final Book book) {
		if (book == null) {
			throw new IllegalArgumentException("Book mustn't be null.");
		}

		if (entityManager.contains(book)) {
			entityManager.remove(book);
		} else {
			entityManager.remove(entityManager.getReference(Book.class, book.getId()));
		}
	}

}

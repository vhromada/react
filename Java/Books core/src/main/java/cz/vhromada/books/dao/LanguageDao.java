package cz.vhromada.books.dao;

import java.util.List;

import cz.vhromada.books.domain.Language;

/**
 * A class represents API for DAO for languages.
 *
 * @author Vladimir Hromada
 */
public interface LanguageDao {

	/**
	 * Returns list of languages.
	 *
	 * @return list of languages
	 */
	List<Language> getLanguages();

	/**
	 * Returns language with ID or null if there isn't such language.
	 *
	 * @param id ID
	 * @return language with ID or null if there isn't such language
	 * @throws IllegalArgumentException if ID is null
	 */
	Language getLanguage(Long id);

	/**
	 * Adds language. Sets new ID and position.
	 *
	 * @param language language
	 * @throws IllegalArgumentException if language is null
	 */
	void add(Language language);

	/**
	 * Updates language.
	 *
	 * @param language language
	 * @throws IllegalArgumentException if language is null
	 */
	void update(Language language);

	/**
	 * Removes language.
	 *
	 * @param language language
	 * @throws IllegalArgumentException if language is null
	 */
	void remove(Language language);

}

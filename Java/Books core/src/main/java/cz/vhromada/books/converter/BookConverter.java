package cz.vhromada.books.converter;

import cz.vhromada.books.domain.Book;
import cz.vhromada.books.to.BookTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * A class represents converter from {@link BookTO} to {@link Book}.
 *
 * @author Vladimir Hromada
 */
@Component("bookConverter")
public class BookConverter implements Converter<BookTO, Book> {

	@Autowired
	private LanguageConverter languageConverter;

	@Override
	public Book convert(final BookTO source) {
		if (source == null) {
			return null;
		}

		final Book book = new Book();
		book.setId(source.getId());
		book.setAuthor(source.getAuthor());
		book.setTitle(source.getTitle());
		book.setIssueYear(source.getIssueYear());
		book.setLanguage(languageConverter.convert(source.getLanguage()));
		book.setPaperForm(source.getPaperForm());
		book.setNote(source.getNote());

		return book;
	}

}

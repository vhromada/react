package cz.vhromada.books.dao;

import java.util.List;

import cz.vhromada.books.domain.Book;

/**
 * A class represents API for DAO for books.
 *
 * @author Vladimir Hromada
 */
public interface BookDao {

	/**
	 * Returns list of books.
	 *
	 * @return list of books
	 */
	List<Book> getBooks();

	/**
	 * Returns book with ID or null if there isn't such book.
	 *
	 * @param id ID
	 * @return book with ID or null if there isn't such book
	 * @throws IllegalArgumentException if ID is null
	 */
	Book getBook(Long id);

	/**
	 * Adds book. Sets new ID and position.
	 *
	 * @param book book
	 * @throws IllegalArgumentException if book is null
	 */
	void add(Book book);

	/**
	 * Updates book.
	 *
	 * @param book book
	 * @throws IllegalArgumentException if book is null
	 */
	void update(Book book);

	/**
	 * Removes book.
	 *
	 * @param book book
	 * @throws IllegalArgumentException if book is null
	 */
	void remove(Book book);

}

package cz.vhromada.books.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * A class represents book.
 *
 * @author Vladimir Hromada
 */
@Entity
@Table(name = "books")
public class Book implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "book_generator", sequenceName = "books_sq", allocationSize = 0)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "book_generator")
	private Long id;

	private String author;

	private String title;

	@Column(name = "issue_year")
	private int issueYear;

	@ManyToOne
	@JoinColumn(name = "language_value", referencedColumnName = "id")
	private Language language;

	@Column(name = "paper_form")
	private Boolean paperForm;

	private String note;

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(final String author) {
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public int getIssueYear() {
		return issueYear;
	}

	public void setIssueYear(final int issueYear) {
		this.issueYear = issueYear;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(final Language language) {
		this.language = language;
	}

	public Boolean getPaperForm() {
		return paperForm;
	}

	public void setPaperForm(final Boolean paperForm) {
		this.paperForm = paperForm;
	}

	public String getNote() {
		return note;
	}

	public void setNote(final String note) {
		this.note = note;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass() || id == null) {
			return false;
		}

		final Book book = (Book) o;

		return id.equals(book.id);
	}

	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : 0;
	}

	@Override
	public String toString() {
		return String.format("Book [id=%d, author=%s, title=%s, issueYear=%d, language=%s, paperForm=%b, note=%s]", id, author, title, issueYear, language,
				paperForm, note);
	}

}

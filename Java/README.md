# Java Rest application

Before you start you have to install

1. [Java 6](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
2. [Maven](http://maven.apache.org/download.cgi).
3. [Derby DB 10.10.2.0](http://db.apache.org/derby/derby_downloads.html)

When you are done just

1. init and launch DB
2. in home project directory run `mvn install` to build application
3. in directory Books REST run `mvn jetty:run` to deploy application to server

Application will be deployed on localhost:8010

REST data will be on

* get all books (GET): localhost:8010/books
* get book by id (GET): localhost:8010/books/{id}
* add book (POST): localhost:8010/books/add
* update book (POST): localhost:8010/books/update
* remove book (POST): localhost:8010/books/remove
* get all languages (GET): localhost:8010/languages
* get language by id (GET): localhost:8010/languages/{id}

package cz.vhromada.books.controller;

import com.owlike.genson.Genson;
import com.owlike.genson.GensonBuilder;
import cz.vhromada.books.LanguageService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * A class represents controller for languages via REST.
 *
 * @author Vladimir Hromada
 */
@Controller("languageController")
@RequestMapping("/languages")
public class LanguageController implements InitializingBean {

	@Autowired
	private LanguageService languageService;

	private Genson genson;

	@Override
	public void afterPropertiesSet() throws Exception {
		genson = new GensonBuilder().create();
	}

	/**
	 * Returns list of languages.
	 *
	 * @return list of languages
	 */
	@RequestMapping(value = { "", "/", "list" }, method = RequestMethod.GET)
	@ResponseBody
	public String getLanguages() {
		return genson.serialize(languageService.getLanguages());
	}

	/**
	 * Returns language with ID or null if there isn't such language.
	 *
	 * @param id ID
	 * @return language with ID or null if there isn't such language
	 * @throws IllegalArgumentException if ID is null
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String getLanguage(@PathVariable("id") final Long id) {
		return genson.serialize(languageService.getLanguage(id));
	}

}

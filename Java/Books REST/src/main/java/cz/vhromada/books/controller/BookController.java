package cz.vhromada.books.controller;

import com.owlike.genson.Genson;
import com.owlike.genson.GensonBuilder;
import cz.vhromada.books.BookService;
import cz.vhromada.books.to.BookTO;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * A class represents controller for books via REST.
 *
 * @author Vladimir Hromada
 */
@Controller("bookController")
@RequestMapping("/books")
public class BookController implements InitializingBean {

	@Autowired
	private BookService bookService;

	private Genson genson;

	@Override
	public void afterPropertiesSet() throws Exception {
		genson = new GensonBuilder().create();
	}

	/**
	 * Returns list of books.
	 *
	 * @return list of books
	 */
	@RequestMapping(value = { "", "/", "list" }, method = RequestMethod.GET)
	@ResponseBody
	public String getBooks() {
		return genson.serialize(bookService.getBooks());
	}

	/**
	 * Returns book with ID or null if there isn't such book.
	 *
	 * @param id ID
	 * @return book with ID or null if there isn't such book
	 * @throws IllegalArgumentException if ID is null
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String getBook(@PathVariable("id") final Long id) {
		return genson.serialize(bookService.getBook(id));
	}

	/**
	 * Adds book. Sets new ID and position.
	 *
	 * @param bookJson JSON book
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public void add(@RequestBody final String bookJson) {
		bookService.add(genson.deserialize(bookJson, BookTO.class));
	}

	/**
	 * Updates book.
	 *
	 * @param bookJson JSON book
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public void update(@RequestBody final String bookJson) {
		bookService.update(genson.deserialize(bookJson, BookTO.class));
	}


	/**
	 * Removes book.
	 *
	 * @param bookJson JSON book
	 */
	@RequestMapping(value = "/remove", method = RequestMethod.POST)
	@ResponseBody
	public void remove(@RequestBody final String bookJson) {
		bookService.remove(genson.deserialize(bookJson, BookTO.class));
	}

}

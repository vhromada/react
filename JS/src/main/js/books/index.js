/** @jsx React.DOM */
var React       = require('react'),
    $           = require('jquery'),
    Router      = require('react-router'),
    Repository  = require('../repository');

var Link = Router.Link;

var Books = React.createClass({

  getInitialState: function () {
    return { books: [] };
  },

  componentDidMount: function () {
    this.fetchAllBooks();
  },

  fetchAllBooks: function () {
    var success = function (books) {
      var newState = { books: books };
      this.setState(newState);
    };
    Repository.BooksRepository.findAll(success.bind(this));
  },

  render: function () {
    var books = this.state.books.map(
      function (book) {
        return (
          <li>
            <Link to="book" id={book.id}>{book.author} - {book.title}</Link>
          </li>
        )
      }
    );

    return  (
      <div>
        <ul>
          {books}
        </ul>
        <Link to="book-add">Add book</Link>
        <br/>
        {this.props.activeRouteHandler ? (this.props.activeRouteHandler() || <span>Please select a book</span>) : <span>Please select a book</span>}
      </div>
    )
  }

});

module.exports = Books;

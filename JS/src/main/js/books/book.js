/** @jsx React.DOM */
var React       = require('react'),
    $           = require('jquery'),
    Router      = require('react-router'),
    Forms       = require('newforms'),
    Repository  = require('../repository');

var Link = Router.Link;

var BookDetail = React.createClass({

  getInitialState: function () {
    return { book: null};
  },

  componentDidMount: function () {
    var id = this.props.params.id;
    var success = function(book) {
      this.setState({ book: book});
    };
    Repository.BooksRepository.findById(id, success.bind(this));
  },

  render: function () {
    var book = this.state.book;
    if (book) {
      return (
        <div>
          <table>
            <thead>
              <tr>
                <td>Author</td>
                <td>Title</td>
                <td>Year of issue</td>
                <td>Language</td>
                <td>Paper form</td>
                <td>Note</td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{book.author}</td>
                <td>{book.title}</td>
                <td>{book.issueYear}</td>
                <td>{book.language.value}</td>
                <td>{book.paperForm ? 'true' : 'false'}</td>
                <td>{book.note}</td>
              </tr>
            </tbody>
          </table>
          <Link to="book-edit" id={book.id}>Edit book</Link>
          <br/>
          <a href="#" onClick={this.remove}>Remove book</a>
          <br/>
          <Link to="books">Back to list</Link>
        </div>
      );
    } else {
      return null;
    }
  },

  remove: function(e) {
    e.preventDefault();
    var success = function() {
      window.location.href = "/";
    }
    Repository.BooksRepository.remove(this.state.book, success);
  }

});

var BookEdit = React.createClass({

  getInitialState: function () {
    return { book: null };
  },

  componentDidMount: function () {
    var id = this.props.params.id;
    var success = function(book) {
      this.setState({ book: book});
    }
    Repository.BooksRepository.findById(id, success.bind(this));
  },

  render: function () {
    var book = this.state.book;
    if (book) {
      return (
        <div>
          <BookEditForm book={book}/>
          <br/>
          <Link to="book" id={book.id}>Back to detail</Link>
        </div>
      );
    } else {
      return null;
    }
  }

});

var BookEditForm = React.createClass({

  getInitialState: function() {
    return { form: null };
  },

  componentDidMount: function() {
    var success = function (languages) {
      var choices = Forms.util.makeChoices(languages, 'id', 'value');
      var book = this.props.book;
      book.language = book.language.id;
      var form = new BookForm(choices, {
        validation: 'auto',
        data: book,
      onStateChange: this.forceUpdate.bind(this)
      });
      var newState = { form: form };
      this.setState(newState);
      return { form: form };
    };
    Repository.LanguagesRepository.findAll(success.bind(this));
  },

  onSubmit: function(e) {
    e.preventDefault();
    var form = this.state.form
    if (form.validate(this.refs.form)) {
      var data = form.cleanedData;
      data.id = this.props.book.id;
      var languageSuccess = function(language) {
          data.language = language;
          var editSuccess = function() {
            window.location.href = "/";
          }
          Repository.BooksRepository.edit(data, editSuccess);
      }
      Repository.LanguagesRepository.findById(data.language, languageSuccess);
    }
  },

  render: function() {
    var form = this.state.form;
    if (form) {
      return (
        <form ref="form" onSubmit={this.onSubmit} method="post">
          {form.asDiv()}
          <div>
            <input type="submit" value="Submit"/>
          </div>
        </form>
      )
    } else {
      return null;
    }
  }

});

var BookAdd = React.createClass({

  render: function () {
    return (
      <div>
        <BookAddForm/>
        <br/>
        <Link to="books">Back to list</Link>
      </div>
    );
  }

});

var BookAddForm = React.createClass({

  getInitialState: function() {
    return { form: null };
  },

  componentDidMount: function() {
    var success = function (languages) {
      var choices = Forms.util.makeChoices(languages, 'id', 'value');
      var form = new BookForm(choices, {
        validation: 'auto',
        onStateChange: this.forceUpdate.bind(this)
      });
      var newState = { form: form };
      this.setState(newState);
      return { form: form };
    };
    Repository.LanguagesRepository.findAll(success.bind(this));
  },

  onSubmit: function(e) {
    e.preventDefault();
    var form = this.state.form
    if (form.validate(this.refs.form)) {
      var data = form.cleanedData;
      var languageSuccess = function(language) {
          data.language = language;
          var addSuccess = function() {
            window.location.href = "/";
          }
          Repository.BooksRepository.add(data, addSuccess);
      }
      Repository.LanguagesRepository.findById(data.language, languageSuccess);
    }
  },

  render: function() {
    var form = this.state.form;
    if (form) {
      return (
        <form ref="form" onSubmit={this.onSubmit} method="post">
          {form.asDiv()}
          <div>
            <input type="submit" value="Submit"/>
          </div>
        </form>
      )
    } else {
      return null;
    }
  }

});

var BookForm = Forms.Form.extend({

  author: Forms.CharField({
    maxLength: 200,
    helpText: 'Author of book'
  }),

  title: Forms.CharField({
    maxLength: 200,
    errorMessages: { required: 'Title is required.' }
  }),

  issueYear: Forms.IntegerField(),

  language: Forms.ChoiceField(),

  paperForm: Forms.BooleanField({ required: false }),

  note: Forms.CharField({ required: false }),

  rowCssClass: 'row',
  errorCssClass: 'error',

  cleanIssueYear: function() {
    var issueYear = this.cleanedData.issueYear;
    if (issueYear < 1800 || issueYear > new Date().getFullYear()) {
      throw Forms.ValidationError('Year must between 1800 and current year.');
    }
  },

  constructor: function(languages, kwargs) {
    BookForm.__super__.constructor.call(this, kwargs);
    this.fields.language.setChoices(languages);
  }

});

module.exports.Detail = BookDetail;
module.exports.Edit = BookEdit;
module.exports.Add = BookAdd;
module.exports.BookEditForm = BookEditForm;
module.exports.BookAddForm = BookAddForm;

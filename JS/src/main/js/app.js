/** @jsx React.DOM */
var React     = window.React = require('react'), // assign it to winow for react chrome extension
    Traceur   = require('traceur-runtime'), // ES6 runtime
    AppRoutes = require('./routes'),
    Header    = require('./header'),
    Books     = require('./books');

// enable touch events
React.initializeTouchEvents(true);

var App = React.createClass({

  render: function () {
    return (
      <div>
        <Header/>
        {this.props.activeRouteHandler() || <Books/>}
      </div>
    )
  }

});

App.start = function (appId) {
  React.renderComponent(<AppRoutes app={App}/>, document.getElementById(appId));
};

module.exports = window.App = App;

var Repository = require('./repository');

class LanguagesRepository extends Repository {

  constructor() {
    super('languages');
  }

}

module.exports = LanguagesRepository;

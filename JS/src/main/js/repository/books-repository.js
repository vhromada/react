var Repository = require('./repository');

class BooksRepository extends Repository {

  constructor() {
    super('books');
  }
  
}

module.exports = BooksRepository;

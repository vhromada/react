var BooksRepository     = require("./books-repository");
var LanguagesRepository = require("./languages-repository");

module.exports.BooksRepository = new BooksRepository();
module.exports.LanguagesRepository = new LanguagesRepository();

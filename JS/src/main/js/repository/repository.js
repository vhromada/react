var rest      = require("rest"),
  mime        = require("rest/interceptor/mime"),
  pathPrefix  = require("rest/interceptor/pathPrefix"),
  entity      = require("rest/interceptor/entity");

var endpoint = "http://localhost:8010";

/**
 * Abstract REST repository.
 */
class Repository {

  constructor(resourcePath) {
    this.path = resourcePath;
    this.client = rest
      .wrap(pathPrefix, {
        prefix: endpoint
      })
      .wrap(mime, {
        mime: 'application/json'
      })
      .wrap(entity);
  }

  findAll(success) {
    this.client({
      path: this.path
    }).then(function(response) {
      success(response);
    });
  }

  findById(id, success) {
    this.client({
      path: this.path + '/' + id
    }).then(function(response) {
      success(response);
    });
  }

  edit(book, success) {
    this.client({
      path: this.path + '/update',
      entity: book
    }).then(function(response) {
      success(response);
    });
  }

  add(book, success) {
    this.client({
      path: this.path + '/add',
      entity: book
    }).then(function(response) {
      success(response);
    });
  }

  remove(book, success) {
    this.client({
      path: this.path + '/remove',
      entity: book
    }).then(function(response) {
      success(response);
    });
  }

}

module.exports = Repository;

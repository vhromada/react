/** @jsx React.DOM */
var Router  = require('react-router'),
    Books   = require('./books'),
    Book    = require('./books/book');

var Routes = Router.Routes;

var Route = Router.Route;

var AppRoutes = React.createClass({

  render: function () {
    return (
      <Routes>
        <Route handler={this.props.app}>
          <Route name="books" handler={Books}>
            <Route name="book" path="/books/book/:id" handler={Book.Detail}/>
            <Route name="book-edit" path="/books/book/:id/edit" handler={Book.Edit}/>
            <Route name="book-add" path="/books/add" handler={Book.Add}/>
          </Route>
        </Route>
      </Routes>
    );
  }

});


module.exports = AppRoutes;

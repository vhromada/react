/** @jsx React.DOM */
var React = require('react');

var Header = React.createClass({

  render: function () {
    return (
      <header>
        <h1>Books</h1>
      </header>
    );
  }

});

module.exports = Header;
